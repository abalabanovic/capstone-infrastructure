
resource "google_compute_instance_template" "instance_template" {
  
    name = "instance-template"
    machine_type = var.machine-type
   

  disk {
  
      source_image = var.image-name
    
  }

    network_interface {

        network = var.vpc_id
        subnetwork = var.subnet_id

        # To get the external IP for VM
        access_config {
          #Leaving empty will give us random public IP
        }

    }

    tags = var.network-tags

    metadata = {
      
      ssh-keys = "ansible:${file("ansible-key.pub")}"

    }

    metadata_startup_script = <<-EOF
        
        sudo adduser ansible
        sudo usermod -aG sudo ansible
         
    EOF


}

resource "google_compute_instance_group_manager" "instance_group_manager" {
  
  name = var.instance-group-name
  zone = var.zone
  base_instance_name = var.base-instance-name
 
  target_size = var.instance-number

  version {
    
    instance_template = google_compute_instance_template.instance_template.self_link
  }

  named_port {
    name = "http"
    port = 80
  }

}



resource "google_sql_database_instance" "petclinic_database_instance" {
  
  name = "petclinic-database-instance"
  database_version = "MYSQL_8_0"
  project = var.project-name
  region = var.region

  settings {

   tier = "db-f1-micro"
  }

 deletion_protection = false

}

resource "google_sql_database" "petclinic_database" {
 
 name = "petclinic"
 instance = google_sql_database_instance.petclinic_database_instance.name
 charset = "utf8"

}

resource "google_sql_user" "users" {

 name = var.username
 instance = google_sql_database_instance.petclinic_database_instance.name
 password =  var.password

}



  
